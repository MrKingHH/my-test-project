#!/bin/bash
set -e

while true;do
awk -v now=`date +%s` -v host=`hostname` '{print "put proc.loadavg.1m"" "now" "$1" ""host="host;
print "put proc.loadavg.5m"" "now" "$3" ""host="host}' /proc/loadavg 
  sleep 5
done | nc -w 10 127.0.0.1 4242
